# ТВиТПО - Курсовая работа

## О задании
Name: Обзор языка TLA+

Author: Andrianov Artemii, Ann Dutova

Group: #5140904/30202

## Цель
Реализовать потокобезопасный канал (очередь) на основе Sequence.

В качестве параметров модель должна принимать:
- `Input` - исходная последовательность данных;
- `ChanLen` - длина последовательности, которая может храниться в канале.

## Решение

### Prerequisites
- Visual Studio Code or Jet Brains Goland
- [TLA+ extensition for VS Code](https://marketplace.visualstudio.com/items?itemName=alygin.vscode-tlaplus) or [TLA+ extention for JB Goland](https://plugins.jetbrains.com/plugin/17965-tla-)
- Go 1.21.4  

### Source code

#### TLA+ / PlusCal source code
The source code of the model in TLA+ language is provided in [`docs/tla`](docs/tla/) directory and consists of:
- [`chan.tla`](docs/tla/chan.tla) source code of the model (module);
- [`chan.cfg`](docs/tla/chan.cfg) configuration specification file.

#### Golang source code
The source code of the model in Golang language is provided in the [`tests`](/tests) directory.

### Tests Golang source code
For run tests use.
``` bash
make test
```




