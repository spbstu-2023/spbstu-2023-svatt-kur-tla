---- MODULE chan ----
EXTENDS Integers, Sequences, TLC

CONSTANTS nil, ChanLen

\* Input data seq.
Input == <<0, 1, 2, 3, 4, 5, 6, 7, 8, 9>>
\* Separation rules for input seq (for 2 writers).
\* Important: start from 1 (not 0)!
InputSegments == <<1, 6, 11>>

\* Writes and readers.
Writers == 1..2
Readers == 3..3

\* New() analog for queue.
MakeQueue == [
    data |-> <<>>,
    mutex |-> nil
]

\* Rule for writer.
CanWrite(chan) ==   /\ chan.mutex = nil
                    /\ Len(chan.data) < ChanLen - 1

\*Rule for reader.
CanRead(chan) ==    /\ chan.mutex = nil
                    /\ Len(chan.data) > 0

\* For match Input and output seq.
MatchAsc(e1, e2) == e1 < e2

(*--algorithm chan

variables
    \* Create new queue.
    chan = MakeQueue,
    \* Create output seq for results.
    output = <<>>
define
    \* Invariants.
    DataLenNoMore == /\ (Len(chan.data) < ChanLen)
    MutexValue == /\ (chan.mutex \in ({nil} \union (Writers \union Readers)))
    \* Success rules (Properties).
    Success == <>[](Len(output) = Len(Input) /\ SortSeq(output, MatchAsc) = SortSeq(Input, MatchAsc))
end define;

\* Block mutex in queue (chan).
macro blockMutex(queue, blocker) begin
    queue.mutex := blocker;
end macro;

\* Unblock mutex in queue (chan).
macro unblockMutex(queue) begin
    queue.mutex := nil;
end macro;

\* Pull data from source seq and push data into target seq.
macro pushWithPull(source, target) begin
    target := Append(target, Head(source));
    source := Tail(source);
end macro;

\* Writers process (can be concurency)
fair+ process Writer \in Writers
variable
    tmp = SubSeq(Input, InputSegments[self], InputSegments[self + 1] - 1),
    isFinish = FALSE;
begin
    WriteData:
        while Len(tmp) > 0 do
            WriterBlockMutex:
                await CanWrite(chan);
                blockMutex(chan, self);
            WriterWriteData:
                await chan.mutex = self;
                assert chan.mutex = self;
                pushWithPull(tmp, chan.data);
                isFinish := TRUE;
            WriterUnblockMutex:
                await chan.mutex = self /\ isFinish = TRUE;
                isFinish := FALSE;
                unblockMutex(chan);
        end while;
end process;

\* Writers process (can not be concurency)
fair+ process Reader \in Readers
variable isFinish = FALSE;
begin
ReadData:
        while Len(Input) /= Len(output) do
            ReaderBlockMutex:
                await CanRead(chan);
                blockMutex(chan, self);
            ReaderReadData:
                await chan.mutex = self;
                assert chan.mutex = self;
                pushWithPull(chan.data, output);
                isFinish := TRUE;
            ReaderUnblockMutex:
                await chan.mutex = self /\ isFinish = TRUE;
                isFinish := FALSE;
                unblockMutex(chan);
        end while;
end process;
end algorithm; *)
\* BEGIN TRANSLATION (chksum(pcal) = "89619ea0" /\ chksum(tla) = "af26c688")
\* Process variable isFinish of process Writer at line 68 col 5 changed to isFinish_
VARIABLES chan, output, pc

(* define statement *)
DataLenNoMore == /\ (Len(chan.data) < ChanLen)
MutexValue == /\ (chan.mutex \in ({nil} \union (Writers \union Readers)))

Success == <>[](Len(output) = Len(Input) /\ SortSeq(output, MatchAsc) = SortSeq(Input, MatchAsc))

VARIABLES tmp, isFinish_, isFinish

vars == << chan, output, pc, tmp, isFinish_, isFinish >>

ProcSet == (Writers) \cup (Readers)

Init == (* Global variables *)
        /\ chan = MakeQueue
        /\ output = <<>>
        (* Process Writer *)
        /\ tmp = [self \in Writers |-> SubSeq(Input, InputSegments[self], InputSegments[self + 1] - 1)]
        /\ isFinish_ = [self \in Writers |-> FALSE]
        (* Process Reader *)
        /\ isFinish = [self \in Readers |-> FALSE]
        /\ pc = [self \in ProcSet |-> CASE self \in Writers -> "WriteData"
                                        [] self \in Readers -> "ReadData"]

WriteData(self) == /\ pc[self] = "WriteData"
                   /\ IF Len(tmp[self]) > 0
                         THEN /\ pc' = [pc EXCEPT ![self] = "WriterBlockMutex"]
                         ELSE /\ pc' = [pc EXCEPT ![self] = "Done"]
                   /\ UNCHANGED << chan, output, tmp, isFinish_, isFinish >>

WriterBlockMutex(self) == /\ pc[self] = "WriterBlockMutex"
                          /\ CanWrite(chan)
                          /\ chan' = [chan EXCEPT !.mutex = self]
                          /\ pc' = [pc EXCEPT ![self] = "WriterWriteData"]
                          /\ UNCHANGED << output, tmp, isFinish_, isFinish >>

WriterWriteData(self) == /\ pc[self] = "WriterWriteData"
                         /\ chan.mutex = self
                         /\ Assert(chan.mutex = self, 
                                   "Failure of assertion at line 77, column 17.")
                         /\ chan' = [chan EXCEPT !.data = Append((chan.data), Head(tmp[self]))]
                         /\ tmp' = [tmp EXCEPT ![self] = Tail(tmp[self])]
                         /\ isFinish_' = [isFinish_ EXCEPT ![self] = TRUE]
                         /\ pc' = [pc EXCEPT ![self] = "WriterUnblockMutex"]
                         /\ UNCHANGED << output, isFinish >>

WriterUnblockMutex(self) == /\ pc[self] = "WriterUnblockMutex"
                            /\ chan.mutex = self /\ isFinish_[self] = TRUE
                            /\ isFinish_' = [isFinish_ EXCEPT ![self] = FALSE]
                            /\ chan' = [chan EXCEPT !.mutex = nil]
                            /\ pc' = [pc EXCEPT ![self] = "WriteData"]
                            /\ UNCHANGED << output, tmp, isFinish >>

Writer(self) == WriteData(self) \/ WriterBlockMutex(self)
                   \/ WriterWriteData(self) \/ WriterUnblockMutex(self)

ReadData(self) == /\ pc[self] = "ReadData"
                  /\ IF Len(Input) /= Len(output)
                        THEN /\ pc' = [pc EXCEPT ![self] = "ReaderBlockMutex"]
                        ELSE /\ pc' = [pc EXCEPT ![self] = "Done"]
                  /\ UNCHANGED << chan, output, tmp, isFinish_, isFinish >>

ReaderBlockMutex(self) == /\ pc[self] = "ReaderBlockMutex"
                          /\ CanRead(chan)
                          /\ chan' = [chan EXCEPT !.mutex = self]
                          /\ pc' = [pc EXCEPT ![self] = "ReaderReadData"]
                          /\ UNCHANGED << output, tmp, isFinish_, isFinish >>

ReaderReadData(self) == /\ pc[self] = "ReaderReadData"
                        /\ chan.mutex = self
                        /\ Assert(chan.mutex = self, 
                                  "Failure of assertion at line 98, column 17.")
                        /\ output' = Append(output, Head((chan.data)))
                        /\ chan' = [chan EXCEPT !.data = Tail((chan.data))]
                        /\ isFinish' = [isFinish EXCEPT ![self] = TRUE]
                        /\ pc' = [pc EXCEPT ![self] = "ReaderUnblockMutex"]
                        /\ UNCHANGED << tmp, isFinish_ >>

ReaderUnblockMutex(self) == /\ pc[self] = "ReaderUnblockMutex"
                            /\ chan.mutex = self /\ isFinish[self] = TRUE
                            /\ isFinish' = [isFinish EXCEPT ![self] = FALSE]
                            /\ chan' = [chan EXCEPT !.mutex = nil]
                            /\ pc' = [pc EXCEPT ![self] = "ReadData"]
                            /\ UNCHANGED << output, tmp, isFinish_ >>

Reader(self) == ReadData(self) \/ ReaderBlockMutex(self)
                   \/ ReaderReadData(self) \/ ReaderUnblockMutex(self)

(* Allow infinite stuttering to prevent deadlock on termination. *)
Terminating == /\ \A self \in ProcSet: pc[self] = "Done"
               /\ UNCHANGED vars

Next == (\E self \in Writers: Writer(self))
           \/ (\E self \in Readers: Reader(self))
           \/ Terminating

Spec == /\ Init /\ [][Next]_vars
        /\ \A self \in Writers : SF_vars(Writer(self))
        /\ \A self \in Readers : SF_vars(Reader(self))

Termination == <>(\A self \in ProcSet: pc[self] = "Done")

\* END TRANSLATION 
====
