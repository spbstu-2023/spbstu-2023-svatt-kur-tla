package tests

import (
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
	"sort"
	"sync"
	"testing"
)

const ChanLen = 3
const WritersCount = 2

// Struct for thread-safe chan.
type MakeChan struct {
	data  []int
	mutex sync.Mutex
}

// Input data slice.
var input = []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}

// Segments for input data slice.
var inputSegments = []int{0, 5, 10}

// Result slice.
var output = make([]int, 0)

// canWriteBlockMutex block mutex, if len of data slice less than ChanLen.
func canWriteBlockMutex(ch *MakeChan) bool {
	if len(ch.data) < ChanLen-1 {
		ch.mutex.Lock()
		return true
	}
	return false
}

// canReadBlockMutex block mutex, if len of data slice more than zero and mutex is unlocked.
func canReadBlockMutex(ch *MakeChan) bool {
	if len(ch.data) > 0 {
		ch.mutex.Lock()
		return true
	}
	return false
}

// unblockMutex unlocks chan mutex.
func unblockMutex(ch *MakeChan) {
	ch.mutex.Unlock()
}

// pushWithPull makes push HEAD into target slice and source slice equal TAIL.
func pushWithPull(source []int, target []int) ([]int, []int) {
	target = append(target, source[0])
	source = source[1:len(source)]
	return source, target
}

// TestChan is main testcase.
func TestChan(t *testing.T) {
	// Create new logger.
	l, err := zap.NewProduction()
	assert.NoError(t, err)
	//Create thread-safe channel.
	ch := &MakeChan{}
	// Create wait group for goroutines.
	wg := sync.WaitGroup{}
	wg.Add(WritersCount + 1)
	// Create new writers goroutines (processes).
	for i := 0; i < WritersCount; i++ {
		go func(i int) {
			writer(i, l, ch)
			defer wg.Done()
		}(i)
	}
	// Create new reader goroutine (process).
	go func() {
		reader(3, l, ch)
		defer wg.Done()
	}()
	wg.Wait()

	l.Info("chan end of work", zap.Any("input", input), zap.Any("output", output))

	// Sort input and output array.
	sort.Ints(input)
	sort.Ints(output)
	// Check results (Success property).
	assert.Equal(t, input, output)
	assert.Equal(t, len(input), len(output))
}

// writer implements the logic of Writer process.
func writer(pc int, l *zap.Logger, ch *MakeChan) {
	// Get data from input based on inputSegments.
	tmp := input[inputSegments[pc]:inputSegments[pc+1]]
	l.Info("writer ready", zap.Int("pc", pc), zap.Any("tmp", tmp))
	// Run cycle while all data from tmp not send into chan.
	for len(tmp) > 0 {
		// Try to block chan mutex.
		if canWriteBlockMutex(ch) {
			l.Info("writer lock mutex", zap.Int("pc", pc))
			// Push data into chan.
			tmp, ch.data = pushWithPull(tmp, ch.data)
			l.Info("writer write data", zap.Any("chan.data", ch.data))
			// Defer: Unblock mutex.
			unblockMutex(ch)
			l.Info("writer unlock mutex", zap.Int("pc", pc))
		}
	}
}

// reader implements the logic of Reader process.
func reader(pc int, l *zap.Logger, ch *MakeChan) {
	l.Info("reader ready", zap.Int("pc", pc))
	// Run cycle while all data from input not send into output.
	for len(input) != len(output) {
		// Try to block chan mutex.
		if canReadBlockMutex(ch) {
			l.Info("reader lock mutex", zap.Int("pc", pc))
			// Push data into chan.
			ch.data, output = pushWithPull(ch.data, output)
			l.Info("output change", zap.Any("chan.data", ch.data), zap.Any("output", output))
			// Unblock mutex.
			unblockMutex(ch)
			l.Info("reader unlock mutex", zap.Int("pc", pc))
		}
	}
}
